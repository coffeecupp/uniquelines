from unique.unique import find_unique_line as ful
import nose


def test_find_line_of_c():
    """
    The line found should be cccccc
    """
    line = ful()
    nose.tools.eq_('cccccc', line.strip('\''))


def test_dont_find_a():
    """
    Don't find a line of aaaaaa
    """
    line = ful()
    nose.tools.assert_not_equals(line.strip('\''), 'aaaaaa')
