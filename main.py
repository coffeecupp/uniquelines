#!/usr/local/bin/python3
import argparse
from unique.unique import find_unique_line


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Find the unique line within a file.")
    parser.add_argument("-f",
                        "--filename",
                        help="Filename to look for the unique line",
                        type=str)
    args = parser.parse_args()
    if args.filename is not None:
        print(find_unique_line(args.filename))
    else:
        print(find_unique_line())
