def find_unique_line(filename: str="testdata.txt") -> str:
    """
    params:
        filename: String representation of the passed in file name.

    returns:
        unique_line: The unique line in the file.
    """
    try:
        lines: List[str] = list(open(filename,'r').readlines())
        unique_line: List[str] = [line.strip('\n') for line in lines
                                  if lines.count(line) == 1]
        return str(unique_line)[1:-1]
    except IOError as err:
        print("IOError thrown {}".format(err))
    except Exception as err:
        print("Exception thrown {}".format(err))
